#include "stdafx.h"
#include "Table.h"
#include <iostream>

Table::Table(int size_of_table)
{
    tab = new int[size_of_table];
    size = size_of_table;
    index = 0;
}

Table::~Table()
{
    delete []tab;
}

void Table::push(int value)
{
    if (index < size)
    {
        tab[index] = value;
        index++;
    }
}

void Table::display()
{
    for (unsigned int i = 0; i < size; i++)
    {
        std::cout << tab[i] << std::endl;
    }
}

unsigned int Table::getSize() const
{
    return size;
}
