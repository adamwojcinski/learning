#include "stdafx.h"
#include "Sort.h"
#include <iostream>

Sort::Sort(int size_of_table):Table(size_of_table){}

Sort::~Sort()
{
}

void Sort::adams_select_sort()
{
    int min;
    int index;
    bool isSwapNeeded;

    for (unsigned int i = 0; i < getSize(); i++)
    {
        min = tab[i];
        isSwapNeeded = false;
        for (unsigned int z = i + 1; z < getSize(); z++)
        {
            if (tab[z] < min)
            {
                isSwapNeeded = true;
                index = z;
                min = tab[z];
            }
        }

        if (isSwapNeeded)
        {
            int temp;
            temp = tab[i];
            tab[i] = tab[index];
            tab[index] = temp;
        }
    }
}

void Sort::select_sort()
{
    int k;
    for (int i = 0; i < getSize(); i++)
    {
        k = i;
        for (int j = i + 1; j < getSize(); j++)
            if (tab[j] < tab[k])
                k = j;

        std::swap(tab[k], tab[i]);
    }
}