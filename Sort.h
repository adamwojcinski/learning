#pragma once
#include "Table.h"
class Sort : public Table
{
public:
    Sort(int size_of_table);
    virtual ~Sort();
    void adams_select_sort();
    void select_sort();
};