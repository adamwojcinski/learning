#pragma once
class Table
{
private:
    unsigned int size;
    unsigned int index;
public:
    int *tab;
    Table(int size_of_table);
    virtual ~Table();
    void push(int value);
    void display();
    unsigned int getSize() const;
};

